﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace dotnet_task2
{
    public partial class Default : Page
    {
        private static readonly List<ProductDetails> productDetailsList = SiteMaster.dataController.GetProductsList();

        public static List<ProductDetails> ProductDetails
        {
            get
            {
                return productDetailsList;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataLoad();
        }

        protected void DataLoad()
        {
            errorMessage.Text = string.Empty;
            try
            {
                for (int i = 0; i < productDetailsList.Count; i++)
                {
                    productDetailsList[i].BrandName = SiteMaster.dataController.GetProductBrand(productDetailsList[i].ProductDetailsId);
                    UserControls.AllProducts uc = (UserControls.AllProducts)Page.LoadControl("~/UserControls/AllProducts.ascx");
                    uc.productDetailsLink = "~/ProductDisplay.aspx?id=" + productDetailsList[i].ProductDetailsId.ToString();
                    uc.productImageSrc = "data:image;base64," + Convert.ToBase64String(productDetailsList[i].ProductImage);
                    uc.productTitleText = productDetailsList[i].ProductName;
                    uc.productTitleLink = "~/ProductDisplay.aspx?id=" + productDetailsList[i].ProductDetailsId.ToString();
                    uc.productBodyText = productDetailsList[i].ProductDesc;
                    itemRow.Controls.Add(uc);
                }
            }
            catch (Exception ex)
            {
                errorMessage.Text = ex.Message;
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
        }

        protected void signup_Click(object sender, EventArgs e)
        {
        }
    }
}