﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Register.aspx.cs" Inherits="dotnet_task2.Register" %>

<asp:Content ID="register_head" ContentPlaceHolderID="head" runat="server">
    <link href="Content/Login.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="register_body" ContentPlaceHolderID="body" runat="server">
    <div class="container">
        <h2 class="text-center">Sign Up</h2>
        <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="firstName" placeholder="First Name"
                runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="firstName" runat="server"
                Display="Dynamic" ForeColor="Red" ErrorMessage="First Name is required" SetFocusOnError="true" />
        </div>
        <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="lastName" placeholder="Last Name"
                runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="lastName" runat="server"
                Display="Dynamic" ForeColor="Red" ErrorMessage="Last Name is required" SetFocusOnError="true" />
        </div>
        <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="email" placeholder="Email" AutoPostBack="true" OnTextChanged="checkEmail_TextChanged"
                runat="server">
            </asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="email" runat="server"
                Display="Dynamic" ForeColor="Red" ErrorMessage="Email is required" SetFocusOnError="true" />
            <asp:RegularExpressionValidator runat="server"
                ControlToValidate="email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                Display="Dynamic" ErrorMessage="Invalid email format" ForeColor="Red"
                SetFocusOnError="true">
            </asp:RegularExpressionValidator>
            <asp:UpdatePanel ID="checkEmail" runat="Server" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="email" EventName="TextChanged" />
                </Triggers>
                <ContentTemplate>
                    <asp:Label ID="checkLabel" runat="server"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="form-group">
            <asp:TextBox TextMode="Password" CssClass="form-control" ID="password" placeholder="Password"
                runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="password" runat="server"
                Display="Dynamic" ForeColor="Red" ErrorMessage="Password is required" SetFocusOnError="true" />
        </div>
        <div class="form-group">
            <asp:TextBox TextMode="Password" CssClass="form-control" ID="retypePassword" placeholder="Re-enter Password"
                runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="retypePassword" runat="server"
                Display="Dynamic" ForeColor="Red" ErrorMessage="Re-entering password is required"
                SetFocusOnError="true" />
            <asp:CompareValidator runat="server"
                Display="Dynamic" ErrorMessage="Passwords dont match"
                ControlToValidate="password" ControlToCompare="retypePassword"
                Type="String" Operator="Equal" ForeColor="Red" SetFocusOnError="true">
            </asp:CompareValidator>
        </div>
        <div class="form-group">
            <asp:DropDownList ID="city" CssClass="form-control" runat="server">
                <asp:ListItem Value="-1">Choose City</asp:ListItem>
                <asp:ListItem>Hyderabad</asp:ListItem>
                <asp:ListItem>Chennai</asp:ListItem>
                <asp:ListItem>Mumbai</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server"
                ErrorMessage="City is required" ForeColor="Red" InitialValue="-1"
                ControlToValidate="city" Display="Dynamic" SetFocusOnError="true">
            </asp:RequiredFieldValidator>
        </div>
        <div class="form-group">
            <asp:Button ID="submit" CssClass="btn btn-primary btn-block" Text="Sign Up" OnClick="submit_Click"
                runat="server" />
        </div>
        <div class="form-group">
            <asp:Label ID="registerMessage" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>