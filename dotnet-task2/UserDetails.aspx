﻿<%@ Page Title="Details" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UserDetails.aspx.cs" Inherits="dotnet_task2.UserDetails" %>

<asp:Content ID="details_head" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="details_body" ContentPlaceHolderID="body" runat="server">
    <dl class="row ml-3 mt-3">
        <dt class="col-sm-3">First Name</dt>
        <dd class="col-sm-9" id="firstName" runat="server"></dd>

        <dt class="col-sm-3">Last Name</dt>
        <dd class="col-sm-9" id="lastName" runat="server"></dd>

        <dt class="col-sm-3">Email</dt>
        <dd class="col-sm-9" id="email" runat="server"></dd>

        <dt class="col-sm-3">City</dt>
        <dd class="col-sm-9" id="city" runat="server"></dd>
    </dl>
</asp:Content>
