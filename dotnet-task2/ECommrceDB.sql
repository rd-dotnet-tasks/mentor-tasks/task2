SET NOCOUNT ON
GO

USE master
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysdatabases WHERE name='ECommerceDB')
BEGIN
	ALTER DATABASE ECommerceDB SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE ECommerceDB
END
GO

CREATE DATABASE ECommerceDB
GO

USE "ECommerceDB"
GO
---------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('dbo.UserLoginInfo') and sysstat & 0xf = 3)
	DROP TABLE "dbo"."UserLoginInfo"
GO
CREATE TABLE UserLoginInfo (
    UserId INT IDENTITY(1,1) PRIMARY KEY,
    UserEmail VARCHAR(100) UNIQUE NOT NULL,
    UserPassword VARCHAR(100) NOT NULL,
)
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[spCheckEmailExists]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE spCheckEmailExists
GO
CREATE PROCEDURE spCheckEmailExists @Email VARCHAR(100)
AS
	RETURN (SELECT COUNT(UserId) FROM UserLoginInfo WHERE UserEmail = @Email)
GO
---------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('dbo.UserDetails') and sysstat & 0xf = 3)
	DROP TABLE "dbo"."UserDetails"
GO
CREATE TABLE UserDetails (
    UserDetailsId INT IDENTITY(1,1) PRIMARY KEY,         
    FirstName VARCHAR(100) NOT NULL,
    LastName VARCHAR(100) NOT NULL,        
    City VARCHAR(100),
    UserId INT FOREIGN KEY REFERENCES UserLoginInfo(UserId)
)
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[spVerifyUserExists]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE spVerifyUserExists
GO
CREATE PROCEDURE spVerifyUserExists @Email VARCHAR(100),@Password VARCHAR(100), @UserId INT out, @UserName VARCHAR(100) out
AS
SET @UserId = (SELECT UserId FROM UserLoginInfo WHERE UserEmail = @Email AND UserPassword = @Password)
SET @UserName = ''
IF @UserId IS NOT NULL
    SET @UserName = (SELECT FirstName FROM UserDetails WHERE UserDetailsId = @UserId) 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[spRegisterUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE spRegisterUser
GO
CREATE PROCEDURE spRegisterUser @Email VARCHAR(100), @Password VARCHAR(100), @FirstName VARCHAR(100), @LastName VARCHAR(100), @City VARCHAR(255)
AS
INSERT INTO UserLoginInfo (UserEmail, UserPassword) VALUES (@Email, @Password)
INSERT INTO UserDetails (FirstName, LastName, City, UserId) VALUES (@FirstName, @LastName, @City,
    (SELECT UserId FROM UserLoginInfo WHERE UserEmail = @Email))
RETURN SCOPE_IDENTITY()
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[spGetUserDetais]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE spGetUserDetais
GO
CREATE PROCEDURE spGetUserDetais @UserDetailsId INT
AS
SELECT UserDetails.FirstName, UserDetails.LastName, UserDetails.City, UserLoginInfo.UserEmail 
from UserDetails, UserLoginInfo where (UserDetails.UserDetailsId = @UserDetailsId and UserDetails.UserId = UserLoginInfo.UserId);
GO
---------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('dbo.BrandDetails') and sysstat & 0xf = 3)
	DROP TABLE "dbo"."BrandDetails"
GO
CREATE TABLE BrandDetails (
    BrandDetailsId INT IDENTITY(1,1) PRIMARY KEY,         
    BrandName VARCHAR(100) UNIQUE NOT NULL,		
)
GO

INSERT INTO BrandDetails(BrandName) VALUES('IndoPrimo'), ('ACME'), ('MAX'), ('FujIFilm'), ('Mi'), ('CAPRESE')
---------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('dbo.ProductDetails') and sysstat & 0xf = 3)
	DROP TABLE "dbo"."ProductDetails"
GO
CREATE TABLE ProductDetails (
	ProductDetailsId INT IDENTITY(1,1) PRIMARY KEY,
    ProductId VARCHAR(100) UNIQUE NOT NULL ,         
    ProductName VARCHAR(255) NOT NULL,		
    ProductDesc VARCHAR(255),
    ProductPrice DECIMAL(38,2) NOT NULL,
	ProductImage IMAGE NOT NULL,
	BrandDetailsId INT FOREIGN KEY REFERENCES BrandDetails(BrandDetailsId)
)
GO

INSERT INTO ProductDetails (ProductId, ProductName, ProductDesc, ProductPrice, ProductImage, BrandDetailsId)
  VALUES 
  ('1GAA1', 'ACME Women''s Slip-On Clog', 'Comfortable and Fashionable', 400, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\one.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'ACME')),
  ('1GAA2', 'IndoPrimo Men''s Cotton Casual Shirt for Men Full Sleeves', 'Slim Fit, High Quality Fabric and Stitching', 2700, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\two.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'MAX')),
  ('1GAA3', 'FujIFilm Instax Mini 9 Instant Camera (Ice Blue)', 'New with selfie mirror', 4199.00, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\three.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'FujIFilm')),
  ('1GAA4', 'MAX Women''s Cotton a-line Kurta', 'Comfortable and Fashionable', 599, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\four.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'MAX')),
  ('1GAA5', 'MAX Rayon Skater Dress', 'Comfortable and Stylish', 959, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\five.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'MAX')),
  ('1GAA6', 'ACME Rapid Leather Safety Shoes Black', 'Comfortable and Light weight', 993, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\six.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'ACME')),
  ('1GAA7', 'ACME Tornado 635 Pealess Whistle', '', 1990, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\seven.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'ACME')),
  ('1GAA8', 'IndoPrimo Silk Saree', 'Trendy and Fashionable', 499, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\eight.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'IndoPrimo')),
  ('1GAA9', 'Mi Band - HRX Edition (Black)', 'OLED display', 1299, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\nine.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'Mi')),
  ('1GAB1', 'Caprese Pepa Women''s Wallet (Aqua)', 'Faux leather material aqua colored wallet', 799, (SELECT * FROM OPENROWSET(BULK N'D:\vs\mentor-tasks\dotnet-task2\dotnet-task2\Images\ten.jpg', SINGLE_BLOB) AS ProductImage), (SELECT BrandDetailsId FROM BrandDetails WHERE BrandName = 'CAPRESE'))

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[spGetProductsList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE spGetProductsList
GO
CREATE PROCEDURE spGetProductsList 
AS
SELECT * FROM ProductDetails 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[spGetProductDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE spGetProductDetails
GO
CREATE PROCEDURE spGetProductDetails @ProductDetailsId INT
AS
SELECT * FROM ProductDetails WHERE ProductDetailsId = @ProductDetailsId
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[spGetProductBrand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE spGetProductBrand
GO
CREATE PROCEDURE spGetProductBrand @ProductDetailsId INT, @BrandName VARCHAR(100) out
AS
SELECT @BrandName = BrandName FROM BrandDetails b
	INNER JOIN ProductDetails p ON b.BrandDetailsId = p.BrandDetailsId where p.ProductDetailsId = @ProductDetailsId 
GO
--------------------------------------------------------------------------------------------------------