﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpecificProduct.ascx.cs"
    Inherits="dotnet_task2.UserControls.SpecificProduct" %>
<div class="row mt-3">
    <div class="col-md-3">
        <small id="productTitle" class="font-weight-bold" runat="server"></small>
        <asp:Image ID="productImage" ClientIDMode="Static" runat="server" ImageUrl="//placehold.it/400x500" CssClass="img-fluid" />
    </div>
    <div class="col-md-9 mt-1">
        <h4 id="productMainTitle" class="font-weight-bold mt-3" runat="server"></h4>
        <small id="productId" runat="server"></small>
        <asp:GridView ID="gvProductDetails" runat="server"
            CssClass="table table-sm table-bordered table-hover mt-3"
            AutoGenerateColumns="False" Width="30%" OnDataBound="gv_OnDataBound">
            <Columns>
                <asp:BoundField DataField="ProductQuantity" HeaderText="Quantity" />
                <asp:BoundField DataField="ProductDiscount" HeaderText="Unit Price" />
            </Columns>
        </asp:GridView>
        <asp:DropDownList ID="productSet" ClientIDMode="Static" CssClass="form-control mr-2" runat="server"
            Width="24%" Style="display: inline" AutoPostBack="true" OnSelectedIndexChanged="productSet_SelectedIndexChanged">
            <asp:ListItem Value="-1">Set</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="set_value" CssClass="form-control" runat="server" Width="5%" Style="display: inline"></asp:TextBox>
        <div class="dropdown mt-2" style="width: 30%;">
            <button style="width: 100%;" class="btn-sm btn-primary dropdown-toggle" type="button"
                data-toggle="dropdown">
                Add to cart
            </button>
        </div>
        <p id="brandName" class="mt-1" runat="server"></p>
    </div>
</div>