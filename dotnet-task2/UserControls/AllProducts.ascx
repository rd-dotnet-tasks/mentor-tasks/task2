﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AllProducts.ascx.cs"
    Inherits="dotnet_task2.UserControls.AllProducts" %>

<div class="col-sm-4 card-top-row">
    <div class="card h-100">
        <asp:HyperLink ID="productDetails" runat="server" NavigateUrl="~/ProductsDisplay.aspx?id=1">
            <asp:Image ID="productImage" runat="server" ImageUrl="//placehold.it/400x200" CssClass="card-img-top img-fluid" />
        </asp:HyperLink>
        <div class="card-body">
            <asp:LinkButton runat="server" ID="productTitle" CssClass="card-title font-weight-bold"
                PostBackUrl="~/ProductsDisplay.aspx?id=1"></asp:LinkButton>
            <p id="productBody" runat="server" class="card-text"></p>
        </div>
    </div>
</div>