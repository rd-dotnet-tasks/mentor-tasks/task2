﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace dotnet_task2.UserControls
{
    public partial class SpecificProduct : UserControl
    {
        public string ProductTitleText
        {
            get { return productTitle.InnerText; }
            set { productTitle.InnerText = value; }
        }

        public string productImageSrc
        {
            get { return productImage.ImageUrl; }
            set { productImage.ImageUrl = value; }
        }

        public string ProductMainTitleText
        {
            get { return productMainTitle.InnerText; }
            set { productMainTitle.InnerText = value; }
        }

        public string ProductIdText
        {
            get { return productId.InnerText; }
            set { productId.InnerText = value; }
        }

        public string BrandName
        {
            get { return brandName.InnerText; }
            set { brandName.InnerText = value; }
        }

        public decimal ProductPrice
        {
            get; set;
        }

        private class ProductDiscountDetails
        {
            public int ProductQuantity
            {
                get; set;
            }

            public decimal ProductDiscount
            {
                get; set;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<ProductDiscountDetails> productQuantityList = new List<ProductDiscountDetails>();
            List<int> quantityList = new List<int>() { 1, 10, 20, 50, 100 };
            foreach (int quantity in quantityList)
            {
                if (quantity == 1)
                    productQuantityList.Add(new ProductDiscountDetails { ProductQuantity = quantity, ProductDiscount = (ProductPrice * quantity) });
                else
                    productQuantityList.Add(new ProductDiscountDetails { ProductQuantity = quantity, ProductDiscount = ((ProductPrice * quantity) - ((ProductPrice * quantity/1000) * quantity)) / quantity });
                if (!IsPostBack)
                    productSet.Items.Add(new ListItem { Text = quantity.ToString(), Value = quantity.ToString() });
            }
            gvProductDetails.DataSource = productQuantityList;
            gvProductDetails.DataBind();
        }

        protected void gv_OnDataBound(object sender, EventArgs e)
        {
            GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            row.Controls.Add(new TableCell() { Text = "Set", ColumnSpan = 2 });
            gvProductDetails.HeaderRow.Parent.Controls.AddAt(1, row);
        }

        protected void productSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (productSet.SelectedValue == "-1")
                set_value.Text = "";
            else set_value.Text = productSet.SelectedValue;
        }
    }
}