﻿using System;
using System.Web.UI;

namespace dotnet_task2.UserControls
{
    public partial class AllProducts : UserControl
    {
        public string productDetailsLink
        {
            get { return productDetails.NavigateUrl; }
            set { productDetails.NavigateUrl = value; }
        }

        public string productImageSrc
        {
            get { return productImage.ImageUrl; }
            set { productImage.ImageUrl = value; }
        }

        public string productTitleText
        {
            get { return productTitle.Text; }
            set { productTitle.Text = value; }
        }

        public string productTitleLink
        {
            get { return productTitle.PostBackUrl; }
            set { productTitle.PostBackUrl = value; }
        }

        public string productBodyText
        {
            get { return productBody.InnerText; }
            set { productBody.InnerText = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}