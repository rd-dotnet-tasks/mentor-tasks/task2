﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AboutUs.aspx.cs" Inherits="dotnet_task2.About" %>

<asp:Content ID="aboutus_head" ContentPlaceHolderID="head" runat="server">
    <link href="Content/AboutUs.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="aboutus_body" ContentPlaceHolderID="body" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 side-panels">
            </div>
            <div class="col-sm">
                <br />
                <h2>Welcome</h2>
                <hr />
                <div class="pageText">EpamShopping India empowers small and medium-sized businesses to reach millions of customers with a number of programmes that help boost their revenue, reach and productivity. By telling stories from a wide range of perspectives, we tell the larger story of who EpamShopping is and how EpamShopping's core business practices contribute to a better India.</div>
                <br />
                <div class="pageText">EpamShopping India unveils long term sustainable packaging initiatives; aims to eliminate single use plastic packaging by June 2020. Paper cushions to replace plastic dunnage in all EpamShopping India Fulfilment Centers by end of the year.</div>
            </div>
            <div class="col-md-2 side-panels">
            </div>
        </div>
    </div>
</asp:Content>