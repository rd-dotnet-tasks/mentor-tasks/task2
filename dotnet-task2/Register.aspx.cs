﻿using System;
using System.Web.Security;

namespace dotnet_task2
{
    public partial class Register : System.Web.UI.Page
    {
        private static readonly UserInfo userInfo = new UserInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Page Load
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            if (IsPostBack && IsValid)
            {
                userInfo.FirstName = firstName.Text;
                userInfo.LastName = lastName.Text;
                userInfo.UserEmail = email.Text;
                userInfo.UserPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password.Text, "SHA1");
                userInfo.City = city.SelectedValue;
                SiteMaster.dataController.RegisterUser(userInfo);
                Response.Redirect("~/Login.aspx");
            }
        }

        protected void checkEmail_TextChanged(object sender, EventArgs e)
        {
            int retVal = SiteMaster.dataController.CheckEmailExists(email.Text);
            if (retVal == 1)
            {
                checkLabel.ForeColor = System.Drawing.Color.Red;
                checkLabel.Text = "An account with the corresponding email already exists";
            }
            else
            {
                checkLabel.Text = String.Empty;
            }
        }
    }
}