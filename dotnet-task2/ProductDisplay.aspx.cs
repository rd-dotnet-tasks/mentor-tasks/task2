﻿using System;
using System.Web.UI;

namespace dotnet_task2
{
    public partial class ProductDisplay : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserControls.SpecificProduct uc = (UserControls.SpecificProduct)Page.LoadControl("~/UserControls/SpecificProduct.ascx");
            if (Int32.TryParse(Request.QueryString["id"], out int id))
                id--;
            if (id >= Default.ProductDetails.Count)
                return;
            uc.ProductTitleText = Default.ProductDetails[id].ProductName;
            uc.productImageSrc = "data:image;base64," + Convert.ToBase64String(Default.ProductDetails[id].ProductImage);
            uc.ProductMainTitleText = Default.ProductDetails[id].ProductName;
            uc.ProductIdText = "Item#: " + Default.ProductDetails[id].ProductId;
            uc.ProductPrice = Default.ProductDetails[id].ProductPrice;
            uc.BrandName = "Brand: " + Default.ProductDetails[id].BrandName;
            productDisplay.Controls.Add(uc);
        }
    }
}