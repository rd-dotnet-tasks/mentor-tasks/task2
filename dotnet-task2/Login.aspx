﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="dotnet_task2.Login" %>

<asp:Content ID="login_head" ContentPlaceHolderID="head" runat="server">
    <link href="Content/Login.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="login_body" ContentPlaceHolderID="body" runat="server">
    <div class="container">
        <h2 class="text-center">Log in</h2>
        <div class="form-group">
            <asp:TextBox ID="email" CssClass="form-control"
                placeholder="Enter email" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="email" runat="server" Display="Dynamic"
                ForeColor="Red" ErrorMessage="Email is required" SetFocusOnError="true" />
            <asp:RegularExpressionValidator runat="server"
                ControlToValidate="email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                Display="Dynamic" ErrorMessage="Invalid email format" ForeColor="Red" SetFocusOnError="true">
            </asp:RegularExpressionValidator>
        </div>
        <div class="form-group">
            <asp:TextBox ID="password" CssClass="form-control" TextMode="Password"
                placeholder="Password" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="password" runat="server" Display="Dynamic"
                ForeColor="Red" ErrorMessage="Password is required" SetFocusOnError="true" />
        </div>
        <div class="form-group">
            <asp:Button ID="submit" CssClass="btn btn-primary btn-block" runat="server" Text="Login"
                OnClick="submit_Click" />
        </div>
        <div class="form-group">
            <asp:Button ID="signup" CssClass="btn btn-outline-secondary btn-block"
                Text="Create an Account" runat="server" OnClick="signup_Click" CausesValidation="false" />
        </div>
        <div class="form-group">
            <asp:Label ID="loginMessage" runat="server"></asp:Label>
        </div>
    </div>
</asp:Content>