﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="dotnet_task2.Default" %>

<asp:Content ID="default_head" ContentPlaceHolderID="head" runat="server">
    <link href="Content/Default.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="default_body" ContentPlaceHolderID="body" runat="server">
    <asp:Label ID="errorMessage" runat="server" />
    <div class="container">
        <div id="itemRow" class="row" runat="server">
        </div>
    </div>
</asp:Content>