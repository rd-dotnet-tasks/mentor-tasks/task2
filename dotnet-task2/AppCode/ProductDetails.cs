﻿namespace dotnet_task2
{
    public class ProductDetails
    {
        public int ProductDetailsId { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDesc { get; set; }
        public decimal ProductPrice { get; set; }
        public byte[] ProductImage { get; set; }
        public int BrandDetailsId { get; set; }
        public string BrandName { get; set; }
    }
}