﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace dotnet_task2
{
    public abstract class DataProvider
    {
        private static string _connectionString = ConfigurationManager.ConnectionStrings["ECommerceDB"].ConnectionString;

        public static string[] VerifyUserExists(UserInfo userInfo)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand("spVerifyUserExists", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@Email", userInfo.UserEmail));
                        sqlCommand.Parameters.Add(new SqlParameter("@Password", userInfo.UserPassword));
                        sqlCommand.Parameters.Add("@UserId", SqlDbType.Int).Direction = ParameterDirection.Output;
                        sqlCommand.Parameters.Add("@UserName", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                        sqlCommand.ExecuteNonQuery();
                        string[] retValues = new string[2];
                        retValues[0] = sqlCommand.Parameters["@UserId"].Value.ToString();
                        retValues[1] = sqlCommand.Parameters["@UserName"].Value.ToString();
                        return retValues;
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static int RegisterUser(UserInfo userInfo)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand("spRegisterUser", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        SqlParameter output = new SqlParameter("@UserId", SqlDbType.Int) { Direction = ParameterDirection.ReturnValue };
                        sqlCommand.Parameters.Add(new SqlParameter("@Email", userInfo.UserEmail));
                        sqlCommand.Parameters.Add(new SqlParameter("@Password", userInfo.UserPassword));
                        sqlCommand.Parameters.Add(new SqlParameter("@FirstName", userInfo.FirstName));
                        sqlCommand.Parameters.Add(new SqlParameter("@LastName", userInfo.LastName));
                        sqlCommand.Parameters.Add(new SqlParameter("@City", userInfo.City));
                        sqlCommand.Parameters.Add(output);
                        sqlCommand.ExecuteScalar();
                        return (int)output.Value;
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static int CheckEmailExists(string userEmail)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand("spCheckEmailExists", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        SqlParameter output = new SqlParameter("@emailExists", SqlDbType.Int) { Direction = ParameterDirection.ReturnValue };
                        sqlCommand.Parameters.Add(new SqlParameter("@Email", userEmail));
                        sqlCommand.Parameters.Add(output);
                        sqlCommand.ExecuteScalar();
                        return (int)output.Value;
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static UserInfo GetUserDetails(int userDetailsId)
        {
            DataTable dataTable = new DataTable();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand("spGetUserDetais", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@UserDetailsId", userDetailsId));
                        using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
                        {
                            dataTable.Load(dataReader);
                        }
                    }
                }
                return Helper.DataTableToList<UserInfo>(dataTable)[0];
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static List<ProductDetails> GetProductsList()
        {
            try
            {
                DataTable dataTable = new DataTable();
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand("spGetProductsList", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
                        {
                            dataTable.Load(dataReader);
                        }
                    }
                }
                return Helper.DataTableToList<ProductDetails>(dataTable);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static List<ProductDetails> GetProductDetails(int producDetailstId)
        {
            try
            {
                DataTable dataTable = new DataTable();
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand("spGetProductDetails", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@ProductDetailsId", producDetailstId));
                        using (SqlDataReader dataReader = sqlCommand.ExecuteReader())
                        {
                            dataTable.Load(dataReader);
                        }
                    }
                }
                return Helper.DataTableToList<ProductDetails>(dataTable);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static string GetProductBrand(int producDetailstId)
        {
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new SqlCommand("spGetProductBrand", sqlConnection))
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add(new SqlParameter("@ProductDetailsId", producDetailstId));
                        sqlCommand.Parameters.Add("@BrandName", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                        sqlCommand.ExecuteNonQuery();
                        return sqlCommand.Parameters["@BrandName"].Value.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}