﻿using System.Collections.Generic;

namespace dotnet_task2
{
    public class DataController
    {
        public string[] VerifyUserExists(UserInfo userInfo)
        {
            return DataProvider.VerifyUserExists(userInfo);
        }

        public int RegisterUser(UserInfo userInfo)
        {
            return DataProvider.RegisterUser(userInfo);
        }

        public UserInfo GetUserDetails(int userDetailsId)
        {
            return DataProvider.GetUserDetails(userDetailsId);
        }

        public int CheckEmailExists(string userEmail)
        {
            return DataProvider.CheckEmailExists(userEmail);
        }

        public List<ProductDetails> GetProductsList()
        {
            return DataProvider.GetProductsList();
        }

        public List<ProductDetails> GetProductDetails(int productId)
        {
            return DataProvider.GetProductDetails(productId);
        }

        public string GetProductBrand(int productId)
        {
            return DataProvider.GetProductBrand(productId);
        }
    }
}