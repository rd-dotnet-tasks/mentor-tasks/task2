﻿using System;
using System.Web.Security;

namespace dotnet_task2
{
    public partial class Login : System.Web.UI.Page
    {
        private static readonly UserInfo userInfo = new UserInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Page Load
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            if (IsPostBack && IsValid)
            {
                userInfo.UserEmail = email.Text;
                userInfo.UserPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password.Text, "SHA1");
                string[] retValues = SiteMaster.dataController.VerifyUserExists(userInfo);
                Int32.TryParse(retValues[0], out int userId);
                string userName = retValues[1];
                if (userId > 0)
                {
                    FormsAuthentication.RedirectFromLoginPage(userId.ToString() + "," + userName, true);
                }
                else if (userId == 0)
                {
                    loginMessage.ForeColor = System.Drawing.Color.Red;
                    loginMessage.Text = "Invalid User Details";
                }
            }
        }

        protected void signup_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Register.aspx");
        }
    }
}