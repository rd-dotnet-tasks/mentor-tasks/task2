﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ProductDisplay.aspx.cs" Inherits="dotnet_task2.ProductDisplay" %>

<asp:Content ID="product_head" ContentPlaceHolderID="head" runat="server">
    <link href="Content/ProductsDisplay.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="product_body" ContentPlaceHolderID="body" runat="server">
    <div id="productDisplay" class="container-fluid display" runat="server">
    </div>
    <div id="itemRow" class="row" runat="server"></div>
</asp:Content>