﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace dotnet_task2
{
    public partial class SiteMaster : MasterPage
    {
        protected internal static readonly DataController dataController = new DataController();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Page Load
            if (HttpContext.Current.User.Identity.Name == string.Empty)
            {
                account.Visible = false;
                cart.Visible = false;
                logout.Visible = false;
            }
            else
            {
                login.Visible = false;
                register.Visible = false;
            }
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }

        protected void Register_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Register.aspx");
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();

            // Clear authentication cookie
            HttpCookie rFormsCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            rFormsCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(rFormsCookie);

            // Clear session cookie
            HttpCookie rSessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            rSessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(rSessionCookie);
            FormsAuthentication.RedirectToLoginPage();
        }
    }
}