﻿using System;
using System.Web;
using System.Web.UI;

namespace dotnet_task2
{
    public partial class UserDetails : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Int32.TryParse(HttpContext.Current.User.Identity.Name.Split(',')[0], out int userDetailsId);
                UserInfo info = SiteMaster.dataController.GetUserDetails(userDetailsId);
                firstName.InnerText = info.FirstName;
                lastName.InnerText = info.LastName;
                email.InnerText = info.UserEmail;
                city.InnerText = info.City;
            }
        }
    }
}